from django.test import TestCase, LiveServerTestCase, Client
from django.urls import resolve
from django.test.client import RequestFactory
import time
from .views import base, new_status
from .models import Status
from .forms import StatusForm
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options


# Create your tests here.
class UnitTest(TestCase):

	def test_url_is_exist(self):
		response = Client().get('/')
		self.assertEqual(response.status_code, 200)

	def test_url_using_base_func(self):
		found = resolve('/')
		self.assertEqual(found.func, base)

	def test_url_using_template(self):
		response = Client().get('/')
		self.assertTemplateUsed(response, 'story7/base.html')

	def test_landing_page_contains_correct_html(self):
		response = Client().get('/')
		html_response = response.content.decode('utf8')
		self.assertIn('Story 7 Azhar', html_response)

	def test_landing_page_does_not_contain_correct_html(self):
		response = Client().get('/')
		html_response = response.content.decode('utf8')
		self.assertNotIn('I am not supposed to be here', html_response)

	def test_model_can_create_new_status(self):
		# Creating a new status
		new_status = Status.objects.create(nama='Django', status='Test Status')
		counting_all_available_status = Status.objects.all().count()
		self.assertEqual(counting_all_available_status, 1)

	def test_form_validation_for_blank_items(self):
		form = StatusForm(data = {'nama': '', 'status': ''})
		self.assertFalse(form.is_valid())
		self.assertEqual(form.errors['status'], ["This field is required."])

	def test_form_post_success_and_render_the_result(self):
		test = 'Anonymous'
		response_post = Client().post('/new', {'nama': test, 'status': test})
		self.assertEqual(response_post.status_code, 302)
		response= Client().get('/')
		html_response = response.content.decode('utf8')
		self.assertIn(test, html_response)

	def test_form_post_error_and_render_the_result(self):
		test = 'Anonymous'
		response_post = Client().post('/new', {'nama': '', 'status': ''})
		self.assertEqual(response_post.status_code, 302)
		response = Client().get('/')
		html_response = response.content.decode('utf8')
		self.assertNotIn(test, html_response)

class FunctionalTest(LiveServerTestCase):
    def setUp(self):
        self.selenium = webdriver.Chrome()
        super(FunctionalTest, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(FunctionalTest, self).tearDown()

    def test_input_status(self):
        selenium = self.selenium
        # Opening the link we want to test
        selenium.get('localhost:8000')
        # Find the form element
        time.sleep(5)

        nama = selenium.find_element_by_id('id_nama')
        status = selenium.find_element_by_id('id_status')
        submit = selenium.find_element_by_id('submit')
        # Fill the form with data
        nama.send_keys('Story 7 Test')
        status.send_keys('YEah Yeah')
        # Submit the form
        submit.send_keys(Keys.RETURN)
        time.sleep(5)
