from django.db import models

# Create your models here.
class Status(models.Model):
    nama = models.CharField(max_length = 20)
    status = models.TextField()

    def __str__(self):
        return self.nama