from django.urls import path
from . import views
from django.http import HttpResponse

appname = 'story7'

urlpatterns = [
    path('', views.base),
    path('new', views.new_status, name='new_status'),
]