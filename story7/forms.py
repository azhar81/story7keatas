from django import forms
from .models import Status

class StatusForm(forms.ModelForm):
    error_messages = {
        'required': 'Please do not leave this empty!',
    }
    nama_attrs = {
        'type': 'text',
        'class': 'status-form-input',
        'placeholder':'Nama:'
    }
    status_attrs = {
        'type': 'text',
        'cols': 40,
        'rows': 8,
        'class': 'status-form-textarea',
        'placeholder':'Status:'
    }

    nama = forms.CharField(label='', required=True, max_length=30, widget=forms.TextInput(attrs=nama_attrs))
    status = forms.CharField(label='', required=True, widget=forms.Textarea(attrs=status_attrs))

    class Meta:
        model = Status
        fields = ["nama", "status"]
