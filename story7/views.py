from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from .models import Status
from .forms import StatusForm

# Create your views here.
def base(request):
    status_list = Status.objects.all()
    response = {
        'status_list' : status_list,
        'status_form' : StatusForm,
    }
    return render(request, 'story7/base.html', response)

def new_status(request):
    form = StatusForm(request.POST or None)

    if( (request.method == 'POST') and form.is_valid() ):
        response = {
            'nama' : request.POST['nama'],
            'status' : request.POST['status'],
        }
        
        status = Status(nama=response['nama'], status=response['status'])
        status.save()
        return HttpResponseRedirect('/')
    else:
        return HttpResponseRedirect('/')
    